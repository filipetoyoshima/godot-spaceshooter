extends Area2D

const speed = 100
const maxLife = 4
var life = maxLife

func _ready():
	add_to_group(game.HITTABLE_GROUP)
	pass

func _process(delta):
	move_local_y(speed * delta)
	pass
	
func takeDamage(damage):
	
	get_node("hitAnimation").play("hit")
	
	life-=damage
	if life <= 0:
		remove_from_group(game.HITTABLE_GROUP)
		get_node("hitAnimation").play("destroy")