extends Area2D

const speedY = -500

func _ready():
	
	set_process(true)
	
	pass

func _process(delta):
	
	move_local_y(delta * speedY)
	
	if position.y < -30:
		queue_free()
		pass
		
	pass

func _on_shot_area_entered(area):
	
	if area.is_in_group(game.HITTABLE_GROUP):
		
		if area.has_method("takeDamage"):
			area.takeDamage(1)
		
		queue_free()
	
	pass
