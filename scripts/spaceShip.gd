extends Node2D

const pre_shot = preload("res://scenes/shot.tscn")
const speedX = 250
const speedY = 175
const screenSizeX = 640
const screenSizeY = 480
const spriteSizeX = 32
const spriteSizeY = 32
const shotBreak = 0.1
var shotTimer = 0
var state = 0
# var state:
#	0 means stopped and
#	1 means thrusting

func _ready():
	set_process(true)
	get_node("sprite/animationPlayer").play("reflectLight")
	pass

func _process(delta):
	
	var xMove = 0
	var yMove = 0
	
	#go right
	if Input.is_action_pressed("ui_right") and position.x < screenSizeX - spriteSizeX / 2:
		xMove += 1
		
	#go down
	if Input.is_action_pressed("ui_left") and position.x > spriteSizeX / 2:
		xMove -= 1
		
	#go up
	if Input.is_action_pressed("ui_up") and position.y > spriteSizeY / 2:
		yMove -= 1
		
	#go down
	if Input.is_action_pressed("ui_down") and position.y < screenSizeY - spriteSizeY / 2:
		yMove += 1
	
	#normalized movement
	if xMove != 0 and yMove != 0:
		position += Vector2(xMove * speedX, yMove * speedY) * delta * 0.7
	else:
		position += Vector2(xMove * speedX, yMove * speedY) * delta

	#animation controllers
	if yMove < 0 and state == 0:
		get_node("thrustAnimator").play("thrust")
		state = 1
	elif yMove >= 0 and state == 1:
		get_node("thrustAnimator").play("notThrust")
		state = 0
	

	#move horizontally >>> not normalized, deprecated <<<
	#move_local_x(delta * speedX * xMove, false)
	
	#move vertically >>> not normalized, deprecated <<<
	#move_local_y(delta * speedY * yMove, false)
	
	#shoot
	if Input.is_action_pressed("ui_shot") and shotTimer <= 0:
		
		Shoot(get_node("rightCannon"))
		Shoot(get_node("leftCannon"))
		
		shotTimer = shotBreak
	
	elif shotTimer > 0:
		
		shotTimer -= delta
		
	pass


func Shoot(node):
	var shot = pre_shot.instance()
	shot.position = node.position + position
	get_owner().add_child(shot)
	pass