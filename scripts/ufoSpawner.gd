extends Node

const pre_greenUfo = preload("res://scenes/greenUfo.tscn")
const worldWidth = 640
const worldLenght = 480
const interval = 3
var intervalTimer = 3

func _ready():
	randomize()
	pass

func _process(delta):
	
	if intervalTimer > 0:
		intervalTimer -= delta
	else:
		GenerateGreenUfo(rand_integer(2, 5))
		intervalTimer = interval
	pass
	
func GenerateGreenUfo (quantity):
	# It's supposed to be constants
	# but Godot don't allow function constants :(
	# (it would be kind ugly too)
	var verticalDist = 20
	var horizontalDist = 20
	
	var firstSpawnX
	var firstSpawnY = -20 * quantity
	
	var orientation = rand_integer(0,1)
	# if 1, it's from right to left '->
	# else, it's from left to right <-'
	
	if orientation == 1:
		firstSpawnX = rand_integer(20, worldWidth - 20 * quantity - 40)
	else:
		firstSpawnX = rand_integer(20 + 20 * quantity, worldWidth - 40)
		orientation = -1
		print(firstSpawnX)
	
	for i in range(quantity):
		var enemy = pre_greenUfo.instance()
		enemy.position.x = firstSpawnX + (20 * orientation * i)
		enemy.position.y = firstSpawnY + 20 * i
		get_owner().add_child(enemy)
	
	pass
	
func rand_integer (minVar, maxVar):
	return randi() % (maxVar + 1 - minVar) + minVar
	pass